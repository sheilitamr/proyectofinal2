const loginForm = document.getElementById('login-form')
const uploads = document.getElementById('uploads')

const login = document.getElementById('login')
const register = document.getElementById('register')
const profile = document.getElementById('profile')
const logout = document.getElementById('logout')

loginForm.addEventListener('submit', (e) => {
    e.preventDefault()
    const loginEmail = e.target.email.value
    const loginPassword = e.target.password.value

    //Loguea a un usuario ya existente. Devuelve una promesa en la que resolvemos los datos. Recibe como parámetro el email y el password
    // firebase.auth().signInWithEmailAndPassword(loginEmail, loginPassword)
    //Si existe y está correcto imprimo los datos
    firebase.auth().signInWithEmailAndPassword(loginEmail, loginPassword)
        //Si existe y está correcto imprimo los datos
        .then(data => {
            console.log(data);
            loginModal.classList.remove('lightbox--show')
        })
        //Si no, sale un error
        .catch(function (error) {
            // Handle Errors here.
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
        });
})

//El observador es para saber cuando el usuario está logueado. El login funciona pero necesitas algo que esté vigilando para hacer los cambios en la web.
firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        // Existe un usuario.
        profile.classList.remove('hide')
        logout.classList.remove('hide')
        login.classList.add('hide')
        register.classList.add('hide')
        //Este puede ser el nombre que aparezca arriba de Welcome...
        const displayName = user.displayName;
        console.log(user.displayName);


    } else {
        //Si no existe, volvemos a mostrar el login y el register
        login.classList.remove('hide')
        register.classList.remove('hide')

        //Evitar navegación a la página de perfil
        if (location.pathname == '/profile.html') {
            location.href = '/'
        }
    }
});

//Ir al la página de profile al hacer click en profile
profile.addEventListener('click', (e) => {
    if (e.target.parentElement.dataset.type == 'profile') {
        location.href = '/profile.html'
    }
})

//Cerrar sesión.
logout.addEventListener('click', (e) => {
    //Función callback que llama al método .signOut()
    firebase.auth().signOut()
    //Cambiamos los iconos para que salgan de nuevo login y register
    profile.classList.add('hide')
    logout.classList.add('hide')
    login.classList.remove('hide')
    register.classList.remove('hide')

})

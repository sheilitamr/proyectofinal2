const registerForm = document.getElementById('register-form')

registerForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const registerEmail = e.target.email.value
    const registerPassword = e.target.password.value

    //Crea un usuario. Devuelve una promesa. Recibe como parámetro el email y el password.
    firebase.auth().createUserWithEmailAndPassword(registerEmail, registerPassword)
        .then(result => {
            return result.user.updateProfile({
                displayName: 'sheila'
            })
        })
        .catch(function (error) {
            // Handle Errors here.
            let errorCode = error.code;
            let errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
        });
})

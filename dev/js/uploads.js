const uploader = document.getElementById('progress')
const fileButton = document.getElementById('file-button')

//Vigila cuando un archivo ha sido seleccionado
if (fileButton) {
    fileButton.addEventListener('change', (e) => {
        //Obtenemos el archivo. Nos permitirá acceder al archivo que seleccionemos cuando se nos abra la ventana de seleccion de archivos
        const file = e.target.files[0]

        //Creamos una referencia de almacenamiento en firebase. Usamos firebase.storage().ref('carpeta'+ file.name).Le tenemos que indicar la estructura en la que queremos colocar los archivos.(nombreCarpeta + nombre archivo seleccionado)

        const storageRef = firebase.storage().ref('fotos/' + file.name)

        //Almacenar la referencia en una variable, es lo que nos va a permitir subir el archivo a esa localización. Para hacerlo usamos el método put(archivo-seleccionado).Este método subirá  el archivo a firebase storage. Nos devuelve una tarea una task Upload y utilizando esa tarea podemos vigilarla para cuando cambie de estado poder ir utilizandola para ir actualizando la barra de estado.

        const task = storageRef.put(file)

        //Para hacerlo utilizamos el método on(), que nos va a permitir en el primero de los parámetros especificar que evento queremos vigilar, en este caso (state_changed) y cuando este se produzca podemos tener 3 tipos de state_changed que vamos a vigilar y lo representaremos por funciones.

        task.on('state_changed',
            //Las funciones progress nos va a notoficar el proceso de subida y cada vez que hay una actualización tenemos un callback, una respuesta con el snapshot con la imágen del estado de progreso. 
            function progress(snapshot) {
                const percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                uploader.value = percentage

            },

            //Nos indicará cuando se produzca algún error
            function error(err) {

            },

            //Nos avisará cuando se haya terminado de completar la subida
            function complete() {

            }
        );

    });
}
